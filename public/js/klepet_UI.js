/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

// Shranjevanje lasntih nadimkov za uporabnike
var mojiNadimkiGledeNaVzdevek = {};

/*
** Preoblikuj prikaz uporabnikov glede na nadimke
**
*/
function vzdevkuDodajNadimek(vhodnoBesedilo) {
  for(var user in mojiNadimkiGledeNaVzdevek) {
    vhodnoBesedilo = vhodnoBesedilo.split(user).join(mojiNadimkiGledeNaVzdevek[user] + ' (' + user + ')');
  }
  return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  sporocilo = vzdevkuDodajNadimek(sporocilo);
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if (sporocilo.indexOf("&#") > -1) {
    return $('<div style="font-weight: bold"></div>').html(sporocilo);  
  } else if (sporocilo.search("http") != -1) {
    return divElementHtmlTekst(sporocilo);  
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();

  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    
    //pošlji sporočilo
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
        
        //preveri, če je v povratnem sporočilu nastavljena nova barva
        if (sistemskoSporocilo.novaBarva) {
          $('#sporocila').css("background-color", sistemskoSporocilo.novaBarva);
          $('#kanal').css("background-color", sistemskoSporocilo.novaBarva);
        }
        
        //izpiši vsebino sistemskega sporočila
        $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo.vsebina));
    }
    
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    
    //preveri za slike
    sporocilo = obdelajSlike(sporocilo);
    
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

/*
* Preverjanje za sliko v besedilu in pripenjanje slike
*/
function obdelajSlike(vhod) {
  var noveBesede = vhod;
  //filter za iskanje ULR slike v besedah
  var imgUrlFilter = new RegExp('\\b(http|https)\\:\\/\\/\\b.*\\.(jpg|gif|png)$');
  
  var besede = vhod.split(' ');
  for(var i in besede) {
    if (imgUrlFilter.test(besede[i])) {
      //če jo najde sestavi doda slike na konec sporočila (vsako v novo vrstico)
      noveBesede = noveBesede + '<br />' +'<img src="' + besede[i] + '" style="width:200px;margin-left:20px;" />';
    }
  }
  return noveBesede;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat in odgovor dodeljevanja nadimka
  socket.on('nadimekOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.napaka == null) {
      mojiNadimkiGledeNaVzdevek[rezultat.uporabnik] = rezultat.nadimek;
      sporocilo = '(Uporabnik ' + rezultat.uporabnik + ' ima sedaj nadimek ' + rezultat.nadimek + ')';
    } else {
      sporocilo = rezultat.napaka;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    //če ima sporočilo spremembo vzdevka, posodobi lokalni seznam nadimkov
    if (sporocilo.novVzdevek) {
      if (mojiNadimkiGledeNaVzdevek[sporocilo.prejsnjiVzdevek]) {
        mojiNadimkiGledeNaVzdevek[sporocilo.novVzdevek] = mojiNadimkiGledeNaVzdevek[sporocilo.prejsnjiVzdevek];
        delete mojiNadimkiGledeNaVzdevek[sporocilo.prejsnjiVzdevek];
      }
    }
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
    
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
    
    // Klik na ime uporabnika v seznamu uporabnikov pošlje zasebno sporočilo v obliki krcanja
    $('#seznam-uporabnikov div').click(function() {
      var povratnoSporocilo;
      var imeIzSeznama = $(this).text();
      
      //če ima nadimek, vzami samo uporabnikov vzdevek
      if ((imeIzSeznama.indexOf('(') + imeIzSeznama.indexOf(')')) > -1) {
        povratnoSporocilo = klepetApp.procesirajUkaz('/krcanje ' + imeIzSeznama.substring(imeIzSeznama.indexOf('(')+1, imeIzSeznama.indexOf(')')));
      } else {
        povratnoSporocilo = klepetApp.procesirajUkaz('/krcanje ' + imeIzSeznama);
      }
      
      if (povratnoSporocilo) {
        $('#sporocila').append(divElementHtmlTekst(povratnoSporocilo.vsebina));
      }
      
      //procesirajVnosUporabnika(klepetApp, socket);
      $('#poslji-sporocilo').focus();
    });
    
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
